// Core
import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// Components
import { Filter } from './components/filter';
import { Head } from './components/head';
import { CurrentDayWeather } from './components/currentDayWeather';
import { Forecast } from './components/forecast';
import { store } from './lib/redux/init/store';

/* Other */
import { useWeather } from './hooks/useWeather';
import { filteredWeatherListActions } from './lib/redux/actions';
import { getFilteredWeatherList } from './lib/redux/selectors';

export const App = () => {
    const filteredWeatherList = useSelector(getFilteredWeatherList);
    const dispatch = useDispatch();
    const { data: weather, isFetchedAfterMount, isFetched } = useWeather();

    const [isSubmitted, setIsSubmitted] = useState(false);

    useEffect(() => {
        if (!filteredWeatherList?.data?.length && Array.isArray(weather)) {
            dispatch(filteredWeatherListActions.setFilteredWeatherList(weather));
        }
    }, [isFetchedAfterMount, weather]);

    return (
        <main>
            <Filter isSubmitted = { isSubmitted } setIsSubmitted = { setIsSubmitted } />
            <Head />
            <CurrentDayWeather />
            <Forecast isSubmitted = { isSubmitted } />
        </main>
    );
};
