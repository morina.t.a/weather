export const fetchify = (isFetched, data) => {
    if (!isFetched && !data) {
        return 'Загрузка...';
    }

    if (data) {
        return data;
    }

    return null;
};
