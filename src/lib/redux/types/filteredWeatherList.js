export const filteredWeatherListTypes = Object.freeze({
    SET_FILTERED_WEATHER_LIST: 'SET_FILTERED_WEATHER_LIST',
});
