// Core
import { combineReducers } from 'redux';

// Reducers
import {
    selectedDayIdReducer as selectedDayId,
    filterReducer as filter,
    filteredWeatherListReducer as filteredWeatherList,
} from '../reducers';

export const rootReducer = combineReducers({
    selectedDayId,
    filter,
    filteredWeatherList,
});
