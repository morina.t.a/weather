// Other
import { filterTypes } from '../types';

const initialState = {
    weatherType:     '',
    min_temperature: '',
    max_temperature: '',
};

export const filterReducer = (state = initialState, action) => {
    switch (action.type) {
    case filterTypes.SET_FILTER: {
        return {
            ...state,
            ...action.payload,
        };
    }
    default: {
        return state;
    }
    }
};
