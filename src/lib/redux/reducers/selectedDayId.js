// Other
import { selectedDayIdTypes } from '../types';

const initialState = { id: '' };

export const selectedDayIdReducer = (state = initialState, action) => {
    switch (action.type) {
    case selectedDayIdTypes.SET_SELECTED_DAY_ID: {
        return {
            ...state,
            id: action.payload,
        };
    }
    default: {
        return state;
    }
    }
};
