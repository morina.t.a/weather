// Other
import { filteredWeatherListTypes } from '../types';

const initialState = { data: [] };

export const filteredWeatherListReducer = (state = initialState, action) => {
    switch (action.type) {
    case filteredWeatherListTypes.SET_FILTERED_WEATHER_LIST: {
        return {
            ...state,
            data: action.payload,
        };
    }
    default: {
        return state;
    }
    }
};
