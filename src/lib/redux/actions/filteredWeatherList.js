// Other
import { filteredWeatherListTypes } from '../types';

export const filteredWeatherListActions = Object.freeze({
    setFilteredWeatherList: (data) => {
        return {
            type:    filteredWeatherListTypes.SET_FILTERED_WEATHER_LIST,
            payload: data,
        };
    },
});
