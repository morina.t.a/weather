// Other
import { filterTypes } from '../types';

export const filterActions = Object.freeze({
    setFilter: (filterData) => {
        return {
            type:    filterTypes.SET_FILTER,
            payload: filterData,
        };
    },
});
