// Other
import { selectedDayIdTypes } from '../types';

export const selectedDayIdActions = Object.freeze({
    setSelectedDayId: (id) => {
        return {
            type:    selectedDayIdTypes.SET_SELECTED_DAY_ID,
            payload: id,
        };
    },
});
