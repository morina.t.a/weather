/* Core */
import { useQuery } from 'react-query';

/* Other */
import { api } from '../api';

export const useWeather = () => {
    const result = useQuery('weather', api.getWeather);

    return result;
};
