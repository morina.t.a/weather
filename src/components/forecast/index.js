/* Core */
import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

/* Components */
import { DayData } from './DayData';

/* Other */
import { useWeather } from '../../hooks/useWeather';
import { selectedDayIdActions, filteredWeatherListActions } from '../../lib/redux/actions';
import { getSelectedDayId, getFilter, getFilteredWeatherList } from '../../lib/redux/selectors';

export const Forecast = ({ isSubmitted }) => {
    const selectedDayId = useSelector(getSelectedDayId);
    const filteredWeatherList = useSelector(getFilteredWeatherList);
    const { weatherType, min_temperature, max_temperature } = useSelector(getFilter);
    const dispatch = useDispatch();

    let weatherForAWeek = filteredWeatherList?.data;
    if (weatherType === '' && min_temperature === '' && max_temperature === '') {
        weatherForAWeek = weatherForAWeek?.slice(0, 7);
    } else {
        weatherForAWeek = weatherForAWeek?.filter((day) => {
            const type = weatherType === '' ? day.type : weatherType;
            const minTemp = min_temperature === '' ? day.temperature : min_temperature;
            const maxTemp = max_temperature === '' ? day.temperature : max_temperature;

            return (
                day.type === type
                && day.temperature >= minTemp
                && day.temperature <= maxTemp);
        }).slice(0, 7);
    }

    useEffect(() => {
        if (isSubmitted) {
            dispatch(filteredWeatherListActions.setFilteredWeatherList(weatherForAWeek));
        }
    }, [isSubmitted]);

    useEffect(() => {
        if (Array.isArray(filteredWeatherList?.data)
            && filteredWeatherList?.data?.length) {
            dispatch(selectedDayIdActions.setSelectedDayId(filteredWeatherList?.data[ 0 ]?.id));
        }
    }, [filteredWeatherList?.data]);

    const handleDayClick = (id) => {
        dispatch(selectedDayIdActions.setSelectedDayId(id));
    };

    const weatherJSX = weatherForAWeek?.map((item) => (
        <DayData
            key = { item.id }
            { ...item }
            dataActive = { selectedDayId === item.id }
            handleDayClick = { handleDayClick } />
    ));

    const weatherListJSX = weatherForAWeek?.length > 0 ? weatherJSX : <p className = 'message'>По заданным критериям нет доступных дней!</p>;

    return (
        <div className = 'forecast' >
            { weatherListJSX }
        </div>
    );
};
