/* Other */
import { format } from 'date-fns';
import { ru } from 'date-fns/locale';

export const DayData = ({
    id, day, type, temperature, rain_probability, humidity, dataActive, handleDayClick,
}) => {
    const date = new Date(day);
    const weekDay = format(date, 'eeee', { locale: ru });
    const weatherType = dataActive ? `day ${type} selected` : `day ${type}`;

    return (
        <div
            className = { weatherType }
            onClick = { () => handleDayClick(id) } >
            <p>{ weekDay }</p>
            <span>{ temperature }</span>
        </div>
    );
};
