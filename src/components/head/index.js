/* Core */
import { useSelector } from 'react-redux';

/* Components */
import { HeadData } from './HeadData';

/* Other */
import { useWeather } from '../../hooks/useWeather';
import { fetchify } from '../../helpers/fetchify';
import { DayData } from '../forecast/DayData';
import { getSelectedDayId, getFilteredWeatherList } from '../../lib/redux/selectors';

export const Head = () => {
    const query = useWeather();
    const filteredWeatherList = useSelector(getFilteredWeatherList);
    const selectedDayId = useSelector(getSelectedDayId);

    let dayWeather = filteredWeatherList?.data;

    dayWeather = dayWeather?.filter((day) => day.id === selectedDayId);

    const headDataJSX = dayWeather?.map((item) => <HeadData key = { item.id } { ...item } />);

    return (
        <div>
            { fetchify(query.isFetched, headDataJSX) }
        </div>
    );
};
