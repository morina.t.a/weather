/* Other */
import { format } from 'date-fns';
import { ru } from 'date-fns/locale';

export const HeadData = ({ day, type }) => {
    const date = new Date(day);
    const weekDay = format(date, 'eeee', { locale: ru });
    const dayOfMonth = format(date, 'd MMMM', { locale: ru });
    const weatherType = `icon ${type}`;

    return (
        <div className = 'head' >
            <div className = { weatherType } />
            <div className = 'current-date' >
                <p>{ weekDay }</p>
                <span>{ dayOfMonth }</span>
            </div>
        </div>
    );
};
