/* Core */
import { useForm } from 'react-hook-form';
import { useSelector, useDispatch } from 'react-redux';

/* Other */
import { useWeather } from '../../hooks/useWeather';
import { filterActions, filteredWeatherListActions } from '../../lib/redux/actions';
import { getFilteredWeatherList } from '../../lib/redux/selectors';

export const Filter = ({ isSubmitted, setIsSubmitted }) => {
    const { data: weather, isFetchedAfterMount, isFetched } = useWeather();
    const filteredWeatherList = useSelector(getFilteredWeatherList);
    const dispatch = useDispatch();

    const form = useForm({
        defaultValues: {
            weatherType:     '',
            min_temperature: '',
            max_temperature: '',
        },
    });

    const submitForm = form.handleSubmit((values) => {
        dispatch(filterActions.setFilter(values));
        setIsSubmitted(true);
    });

    const resetForm = form.handleSubmit((values) => {
        form.reset();
        dispatch(filterActions.setFilter({
            weatherType:     '',
            min_temperature: '',
            max_temperature: '',
        }));
        dispatch(filteredWeatherListActions.setFilteredWeatherList(weather));
        setIsSubmitted(false);
    });

    const weatherType = form.watch('weatherType');
    let checkboxCloudy = weatherType === 'cloudy' ? 'checkbox selected' : 'checkbox';
    let checkboxSunny = weatherType === 'sunny' ? 'checkbox selected' : 'checkbox';
    if (form.formState.isSubmitSuccessful) {
        checkboxCloudy += ' blocked';
        checkboxSunny += ' blocked';
    }

    return (
        <form onSubmit = { submitForm } onReset = { resetForm } >
            <div className = 'filter' >
                <span
                    className = { checkboxCloudy }
                    onClick = { (event) => {
                        if (!isSubmitted) {
                            form.setValue('weatherType', 'cloudy', { shouldDirty: true });
                        } else {
                            event.preventDefault();
                        }
                    } } >
                    Облачно
                </span>
                <span
                    className = { checkboxSunny }
                    onClick = { (event) => {
                        if (!isSubmitted) {
                            form.setValue('weatherType', 'sunny', { shouldDirty: true });
                        } else {
                            event.preventDefault();
                        }
                    } } >
                    Солнечно
                </span>
                <p className = 'custom-input' >
                    <label>Минимальная температура</label>
                    <input
                        disabled = { isSubmitted }
                        id = 'min-temperature'
                        name = 'min_temperature'
                        type = 'number'
                        { ...form.register('min_temperature') } />
                </p>
                <p className = 'custom-input' >
                    <label>Максимальная температура</label>
                    <input
                        disabled = { isSubmitted }
                        id = 'max-temperature'
                        name = 'max_temperature'
                        type = 'number'
                        { ...form.register('max_temperature') } />
                </p>
                <button
                    style = { {
                        display: isSubmitted ?  'none' : 'block',
                    } }
                    disabled = { !form.formState.isDirty }
                    type = 'submit' >
                    Отфильтровать
                </button>
                <button
                    style = { {
                        display: isSubmitted ?  'block' : 'none',
                    } }
                    type = 'reset' >
                    Сбросить
                </button>
            </div>
        </form>
    );
};
