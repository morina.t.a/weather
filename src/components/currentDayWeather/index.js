/* Core */
import { useSelector } from 'react-redux';

/* Other */
import { useWeather } from '../../hooks/useWeather';
import { fetchify } from '../../helpers/fetchify';
import { DayData } from '../forecast/DayData';
import { getSelectedDayId, getFilteredWeatherList } from '../../lib/redux/selectors';

export const CurrentDayWeather = () => {
    const query = useWeather();
    const filteredWeatherList = useSelector(getFilteredWeatherList);
    const selectedDayId = useSelector(getSelectedDayId);
    const dayWeather = filteredWeatherList?.data?.find((day) => day.id === selectedDayId);

    const currentDataJSX = (
        <div className = 'current-weather'>
            <p className = 'temperature' >{ dayWeather?.temperature }</p>
            <p className = 'meta' >
                <span className = 'rainy' >%{ dayWeather?.rain_probability }</span>
                <span className = 'humidity' >%{ dayWeather?.humidity }</span>
            </p>
        </div>
    );

    return (
        <div>
            { dayWeather && fetchify(query.isFetched, currentDataJSX) }
        </div>
    );
};
